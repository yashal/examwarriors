package com.examwarriors

import android.app.Application
import com.examwarriors.data.db.AppDataBase
import com.examwarriors.data.network.MyApi
import com.examwarriors.data.network.NetworkConnectionInterceptor
import com.examwarriors.data.preferences.PreferenceProvider
import com.examwarriors.data.repository.QuotesRepository
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.ui.modelfactory.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ExamWarriors : Application(), KodeinAware {


    override val kodein = Kodein.lazy {
        import(androidXModule(this@ExamWarriors))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { AppDataBase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { UserRepository(instance(), instance()) }
        bind() from provider { ProfileViewModelFactory(instance(), instance()) }
        bind() from singleton { QuotesRepository(instance(), instance(), instance()) }
        bind() from provider { QuotesViewModelFactory(instance(), instance()) }
        bind() from provider { CheckEmailViewModelFactory(instance(), instance()) }
        bind() from provider { AuthViewModelFactory(instance(), instance()) }
        bind() from provider { HomeViewModelFactory(instance()) }

    }

    override fun onCreate() {
        super.onCreate()

    }
}