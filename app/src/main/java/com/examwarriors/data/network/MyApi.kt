package com.examwarriors.data.network

import com.examwarriors.data.db.entities.User
import com.examwarriors.data.network.response.AuthResponse
import com.examwarriors.data.network.response.QuotesResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MyApi {
//
//    @FormUrlEncoded
//    @POST("login")
//    suspend fun userLogin(
//        @Field("email") email: String,
//        @Field("password") password: String
//    ): Response<LoginSignupResponse>

    @GET("quotes")
    suspend fun getQuotes(): Response<QuotesResponse>


    @POST("loginCheck.php")
    suspend fun checkUser(@Body user: User): Response<AuthResponse>

    @POST("login.php")
    suspend fun userLogin(@Body user: User): Response<AuthResponse>

    @POST("register.php")
    suspend fun userSignUp(@Body user: User): Response<AuthResponse>


    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://www.online-solutions.in/demo/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)
        }
    }
}