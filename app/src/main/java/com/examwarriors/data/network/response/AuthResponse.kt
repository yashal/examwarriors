package com.examwarriors.data.network.response

import com.examwarriors.data.db.entities.User

data class AuthResponse(
    val error: Int,
    val count: Int,
    val message: String,
    val api_element: String,
    val result: User
)