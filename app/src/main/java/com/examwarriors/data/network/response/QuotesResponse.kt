package com.examwarriors.data.network.response

import com.examwarriors.data.db.entities.Quote

data class QuotesResponse (
    val isSuccessful: Boolean,
    val quotes: List<Quote>
)