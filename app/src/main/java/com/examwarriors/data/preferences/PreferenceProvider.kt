package com.examwarriors.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

private const val KEY_SAVED_AT = "key_saved_at"
const val LOGIN_FROM = "login_from"

class PreferenceProvider(
    var context: Context
) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun savelastSavedAt(savedAt: String) {
        preference.edit().putString(
            KEY_SAVED_AT,
            savedAt
        ).apply()
    }

    fun getLastSavedAt(): String? {
        return preference.getString(KEY_SAVED_AT, null)
    }

    fun put(key: String, value: String) {
        preference.edit().putString(
            key,
            value
        ).apply()
    }

    fun getString(key: String): String? {
        return preference.getString(key, null)
    }

    fun putInt(key: String, value: Int) {
        preference.edit().putInt(
            key,
            value
        ).apply()
    }

    fun getInt(key: String, defaultValue: Int): Int? {
        return preference.getInt(key, defaultValue)
    }

    fun putBoolean(key: String, value: Boolean) {
        preference.edit().putBoolean(
            key,
            value
        ).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean? {
        return preference.getBoolean(key, defaultValue)
    }

    fun putLong(key: String, value: Long) {
        preference.edit().putLong(
            key,
            value
        ).apply()
    }

    fun getLong(key: String, defaultValue: Long): Long? {
        return preference.getLong(key, defaultValue)
    }


    fun clearAllPref(): Boolean {
        val editor = preference.edit()
        editor.clear()
        return editor.commit()
    }

}