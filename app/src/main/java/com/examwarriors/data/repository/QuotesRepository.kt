package com.examwarriors.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.examwarriors.data.db.AppDataBase
import com.examwarriors.data.db.entities.Quote
import com.examwarriors.data.network.MyApi
import com.examwarriors.data.network.SafeApiRequest
import com.examwarriors.data.preferences.PreferenceProvider
import com.examwarriors.utils.Coroutines
import com.examwarriors.utils.getCurrentTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private val MIN_INTERVAL = 60 * 60 * 6

class QuotesRepository(
    private val api: MyApi,
    private val db: AppDataBase,
    private val prefs: PreferenceProvider
): SafeApiRequest() {
    private val quotes = MutableLiveData<List<Quote>>()

    init {
        quotes.observeForever {
            saveQuotes(it)
        }
    }

    suspend fun getQuotes(): LiveData<List<Quote>>{
        return withContext(Dispatchers.IO){
            fetchQuotes()
            db.getQuoteDao().getQuotes()
        }
    }

    private suspend fun fetchQuotes(){
        val lastSavedAt = prefs.getLastSavedAt()
        if (lastSavedAt == null || ifFetchNeeded(lastSavedAt.toLong())){
            val response = apiRequest{ api.getQuotes() }
            quotes.postValue(response.quotes)
        }
    }

    private fun ifFetchNeeded(savedAt: Long): Boolean{
        val difference = savedAt - getCurrentTime()
        return difference > MIN_INTERVAL
    }

    private fun saveQuotes(quotes: List<Quote>){
        Coroutines.io{
            prefs.savelastSavedAt(getCurrentTime().toString())
            db.getQuoteDao().saveAllQuotes(quotes)
        }
    }
}