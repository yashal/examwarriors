package com.examwarriors.data.repository

import com.examwarriors.data.db.AppDataBase
import com.examwarriors.data.db.entities.User
import com.examwarriors.data.network.MyApi
import com.examwarriors.data.network.SafeApiRequest
import com.examwarriors.data.network.response.AuthResponse

class UserRepository(
    private val api: MyApi,
    private val db: AppDataBase
) : SafeApiRequest() {

    suspend fun saveUser(user: User) = db.getUserDao().upsert(user)

    fun getUser() = db.getUserDao().getUser()
    suspend fun deleteUser() = db.getUserDao().deleteUser()


    suspend fun checkUser(email: String): AuthResponse {
        var user = User()
        user.email = email
        return apiRequest { api.checkUser(user) }
    }

    suspend fun doUserLogin(email: String, password: String): AuthResponse {
        val user = User()
        user.email = email
        user.password = password
        return apiRequest { api.userLogin(user) }
    }

    suspend fun doUserSignUp(email: String, password: String, mobile: String, name: String):
            AuthResponse {
        val user = User()
        user.email = email
        user.password = password
        user.name = name
        user.mobile = mobile
        return apiRequest { api.userSignUp(user) }
    }



}