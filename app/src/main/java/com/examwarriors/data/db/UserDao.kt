package com.examwarriors.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.examwarriors.data.db.entities.CURRENT_USER_ID
import com.examwarriors.data.db.entities.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(user: User): Long

    @Query("SELECT * FROM user WHERE uid = $CURRENT_USER_ID")
    fun getUser(): LiveData<User>

    @Query("DELETE FROM user")
    suspend fun deleteUser()


}