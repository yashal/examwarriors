package com.examwarriors.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

const val CURRENT_USER_ID = 0

@Entity
data class User (
    var userId: Int? = null,
    var name: String? = null,
    var email: String? = null,
    var created_at: String? = null,
    var updated_at: String? =  null,
    var userImageURL: String? =  null,
    var userFound: Boolean? = null,
    var password: String? = null,
    var token: String? = null,
    var mobile: String? = null,
    var isPaid: Boolean? = null,
    var selectedInterest: String? = null,
    var typeOfSubscription: String? = null
){
    @PrimaryKey(autoGenerate = false)
    var uid: Int = CURRENT_USER_ID
}