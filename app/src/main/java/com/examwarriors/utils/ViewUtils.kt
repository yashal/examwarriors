package com.examwarriors.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.DialogInterface
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import com.examwarriors.R
import com.google.android.material.snackbar.Snackbar
import java.util.HashMap
import java.util.regex.Pattern

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun ProgressBar.show(){
    visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    visibility = View.GONE
}

fun View.showSnackBar(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also {snackbar ->  
        snackbar.setAction("OK"){
            snackbar.dismiss()
        }
    }.show()
}

fun getCurrentTime(): Long{
    val currentEpochTIme = System.currentTimeMillis()
    return currentEpochTIme / 1000
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun isValidEmail(email: String): Boolean {
    val EMAIL_PATTERN =
        "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

    val pattern = Pattern.compile(EMAIL_PATTERN)
    val matcher = pattern.matcher(email)
    return matcher.matches()
}

fun getMobileNumberVerification(phoneNo: String): HashMap<Boolean, String> {
    val validityMap = HashMap<Boolean, String>()
    val regexStr = "^[6-9][0-9]{9}$"
    val pattern = Pattern.compile(regexStr)
    val matcher = pattern.matcher(phoneNo)
    if (phoneNo.length < 10) {
        validityMap[false] = "Please enter a 10 digit mobile number"
    } else if (!matcher.matches()) {
        validityMap[false] = "Mobile Number should starts with 6 or later"
    } else {
        validityMap[true] = "valid"
    }
    return validityMap
}

fun showAlertDialog(
    context: Context,
    title: String,
    message: String,
    listener: DialogInterface.OnClickListener
): AlertDialog? {
    var title = title
    if (context is Activity && !context.isFinishing) {
        val alertDialog: AlertDialog =
            AlertDialog.Builder(context, R.style.ThemeOverlay_AppCompat_Dialog_Alert).create()

        title =
            if (TextUtils.isEmpty(title)) context.getResources().getString(R.string.app_name) else title

        // Setting Dialog Title
        alertDialog.setTitle(title)

        // Setting Dialog Message
        alertDialog.setMessage(message)

        // Setting alert dialog icon
        // alertDialog.setIcon((status) ? R.drawable.success :
        // R.drawable.fail);

        // Setting Cancel Button
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", listener)

        // Setting OKAY Button
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Okay", listener)

        // Showing Alert Message
        alertDialog.show()

        return alertDialog
    }
    return null
}