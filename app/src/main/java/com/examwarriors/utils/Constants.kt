package com.examwarriors.utils

object Constants {

    const val NAME = "name"
    const val EMAIL = "email"
    const val PROFILE_PIC = "profilePic"
    const val STATUS = "status"

}