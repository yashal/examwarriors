package com.examwarriors.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.examwarriors.data.db.entities.User
import com.examwarriors.ui.activity.*

object ModuleMaster {

    fun navigateToPreLoginPage(context: Context) {
        Intent(context, PreLoginActivity::class.java).also {
            context.startActivity(it)
            (context as Activity).finish()
        }
    }

    fun navigateToMainActivity(context: Context, user: User){
        Intent(context, MainActivity::class.java).also {
            it.putExtra(Constants.NAME, user.name)
            it.putExtra(Constants.EMAIL, user.email)
            it.putExtra(Constants.PROFILE_PIC, user.userImageURL)
            context.startActivity(it)
            (context as Activity).finish()
        }
    }

    fun navigateToAuthActivity(context: Context, user: User){
        val isUserFound = user.userFound
        val name = user.name
        val email = user.email
        Intent(context, AuthActivity::class.java).also {
            it.putExtra(Constants.STATUS, isUserFound)
            it.putExtra(Constants.NAME, name)
            it.putExtra(Constants.EMAIL, email)
            context.startActivity(it)
            (context as Activity).finish()
        }
    }

    fun navigateToLearningActivity(context: Context){
        Intent(context, LearningActivity::class.java).also {
            context.startActivity(it)
        }
    }

    fun navigateToCheckEmailActivity(context: Context){
        Intent(context, CheckEmailActivity::class.java).also {
            context.startActivity(it)
            (context as Activity).finish()
        }
    }

    fun navigateToCheckEmailActivity(context: Context, email: String){
        Intent(context, CheckEmailActivity::class.java).also {
            it.putExtra(Constants.EMAIL, email)
            context.startActivity(it)
            (context as Activity).finish()
        }
    }

}