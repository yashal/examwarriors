package com.examwarriors.ui.adapters

import com.examwarriors.R
import com.examwarriors.data.db.entities.Quote
import com.examwarriors.databinding.ItemQuotesBinding
import com.xwray.groupie.databinding.BindableItem

class QuoteItem(
    private val quote: Quote
): BindableItem<ItemQuotesBinding>() {
    override fun getLayout() = R.layout.item_quotes

    override fun bind(viewBinding: ItemQuotesBinding, position: Int) {
        viewBinding.setQuote(quote)
    }

}