package com.examwarriors.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.examwarriors.ui.modelfactory.AuthViewModelFactory
import com.examwarriors.ui.viewmodel.AuthViewModel
import com.examwarriors.utils.ModuleMaster.navigateToMainActivity
import com.examwarriors.utils.ModuleMaster.navigateToPreLoginPage
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SplashActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance()

    private var splashTimeHandler: Handler? = null
    private var finalizer: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)

        splashTimeHandler = Handler()
        finalizer = Runnable {

           viewModel.getLoggedInUser().observe(this, Observer { user ->
                if (user != null) {
                    navigateToMainActivity(this, user)
                } else {
                    navigateToPreLoginPage(this)
                }

            })
        }
        splashTimeHandler!!.postDelayed(finalizer, 2000)
    }

}
