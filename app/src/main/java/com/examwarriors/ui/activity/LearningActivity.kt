package com.examwarriors.ui.activity

import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.examwarriors.R

class LearningActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar
    private lateinit var title: TextView
    private lateinit var info: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learning)

        setToolBar()
    }

    private fun setToolBar() {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        info = findViewById(R.id.info)
        info.visibility = View.VISIBLE
        title.text = "Learning"
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_black)
        setSupportActionBar(toolbar)
        toolbar.navigationIcon!!.setColorFilter(
            resources.getColor(R.color.white),
            PorterDuff.Mode.SRC_ATOP
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
