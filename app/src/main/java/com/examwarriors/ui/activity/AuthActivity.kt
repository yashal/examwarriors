package com.examwarriors.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.examwarriors.R
import com.examwarriors.data.db.entities.User
import com.examwarriors.data.preferences.LOGIN_FROM
import com.examwarriors.data.preferences.PreferenceProvider
import com.examwarriors.databinding.ActivityAuthBinding
import com.examwarriors.ui.modelfactory.AuthViewModelFactory
import com.examwarriors.ui.viewmodel.AuthViewModel
import com.examwarriors.utils.Constants.EMAIL
import com.examwarriors.utils.Constants.NAME
import com.examwarriors.utils.Constants.STATUS
import com.examwarriors.utils.Coroutines
import com.examwarriors.utils.ModuleMaster.navigateToCheckEmailActivity
import com.examwarriors.utils.ModuleMaster.navigateToMainActivity
import com.examwarriors.utils.hide
import com.examwarriors.utils.show
import com.examwarriors.utils.showSnackBar
import kotlinx.android.synthetic.main.activity_auth.progress_bar
import kotlinx.android.synthetic.main.activity_auth.root_layout
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AuthActivity : AppCompatActivity(), KodeinAware {


    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance()
    private val prefs: PreferenceProvider by instance()

    private lateinit var toolbar: Toolbar
    private var user = User()
    var name: String? = null
    var email: String? = null
    var status: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityAuthBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_auth)

        if (intent.getStringExtra(NAME) != null) {
            name = intent.getStringExtra(NAME)
        }
        if (intent.getStringExtra(EMAIL) != null) {
            email = intent.getStringExtra(EMAIL)
        }
        if (intent.getBooleanExtra(STATUS, false) != null) {
            status = intent.getBooleanExtra(STATUS, false)
        }

        val viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)

        user.name = name
        user.email = email
        user.userFound = status

        binding.user = user
        binding.viewModel = viewModel

        setToolbar()

        viewModel.getLoggedInUser().observe(this, Observer { user ->
            if (user != null) {
                prefs.put(LOGIN_FROM, "InApp")
                navigateToMainActivity(this, user)
            }
        })

        viewModel.onStarted.observe(this, Observer {
            progress_bar.show()
        })

        viewModel.onSuccess.observe(this, Observer {
            Coroutines.main {
                progress_bar.hide()
            }
        })

        viewModel.onFailure.observe(this, Observer {
            Coroutines.main {
                progress_bar.hide()
                root_layout.showSnackBar(it)
            }
        })

        viewModel.onSuccessFullyRegistered.observe(this, Observer {
            Coroutines.main {
                progress_bar.hide()
                onBackPressed()
            }
        })
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = ""
        toolbar.setBackgroundColor(resources.getColor(R.color.page_bg_color))
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_black)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        navigateToCheckEmailActivity(this, email!!)
    }
}
