package com.examwarriors.ui.activity

import android.content.DialogInterface
import android.graphics.PorterDuff
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.examwarriors.R
import com.examwarriors.data.preferences.LOGIN_FROM
import com.examwarriors.data.preferences.PreferenceProvider
import com.examwarriors.ui.fragment.*
import com.examwarriors.ui.modelfactory.AuthViewModelFactory
import com.examwarriors.ui.viewmodel.AuthViewModel
import com.examwarriors.utils.CircleTransform
import com.examwarriors.utils.Constants.EMAIL
import com.examwarriors.utils.Coroutines
import com.examwarriors.utils.toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_main.toolbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import com.examwarriors.utils.Constants.NAME
import com.examwarriors.utils.Constants.PROFILE_PIC
import com.examwarriors.utils.ModuleMaster.navigateToPreLoginPage
import com.examwarriors.utils.showAlertDialog
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class MainActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance()
    private val prefs: PreferenceProvider by instance()

    lateinit var drawer: DrawerLayout
    lateinit var bottomNav: BottomNavigationView
    lateinit var name: String
    lateinit var email: String
    private var doubleBackToExitPressedOnce = false
    lateinit var navController: NavController
    lateinit var viewModel: AuthViewModel
    lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(
            R.layout.activity_main
        )

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(resources.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)


        drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        bottomNav = findViewById<BottomNavigationView>(R.id.bottomNav)

        setSupportActionBar(toolbar)

        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        navController = Navigation.findNavController(this, R.id.fragment)

        bottomNav.setupWithNavController(navController)
        NavigationUI.setupWithNavController(nav_view, navController)
        NavigationUI.setupActionBarWithNavController(this, navController, drawer)

        toolbar.navigationIcon!!.setColorFilter(
            resources.getColor(R.color.white),
            PorterDuff.Mode.SRC_ATOP
        );

        val headerView = navigationView.getHeaderView(0)
        val navUserName = headerView.findViewById(R.id.header_name) as TextView
        val navUserEmail = headerView.findViewById(R.id.header_email) as TextView
        val navUserImage = headerView.findViewById(R.id.imageView) as ImageView

        if (intent.getStringExtra(NAME) != null) {
            name = intent.getStringExtra(NAME)
            navUserName.text = name
        }
        if (intent.getStringExtra(EMAIL) != null) {
            email = intent.getStringExtra(EMAIL)
            navUserEmail.text = email
        }
        if (intent.getStringExtra(PROFILE_PIC) != null) {
            var profilePic = intent.getStringExtra(PROFILE_PIC)
            if (!profilePic.isNullOrEmpty())
                Picasso.get()
                    .load(profilePic)
                    .placeholder(R.drawable.ic_app_logo)
                    .error(R.drawable.ic_app_logo)
                    .transform(CircleTransform())
                    .into(navUserImage);
        }

        setupDrawerContent(navigationView)
        setUpBottomContent(bottomNav)
    }

    private fun setUpBottomContent(bottomNavigation: BottomNavigationView) {
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment)
            var fragment = navHostFragment?.getChildFragmentManager()!!.fragments[0]
            when (item.itemId) {
                R.id.homeFragment -> {
                    if (fragment !is HomeFragment) {
                        NavigationUI.onNavDestinationSelected(item, navController)
                    } else {
                        false
                    }
                }
                R.id.profileFragment -> {
                    if (fragment !is ProfileFragment) {
                        NavigationUI.onNavDestinationSelected(item, navController)
                    } else {
                        false
                    }

                }
                R.id.mySubscriptionFragment -> {
                    if (fragment !is MySubscriptionFragment) {
                        NavigationUI.onNavDestinationSelected(item, navController)
                    } else {
                        false
                    }
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment)
            var fragment = navHostFragment?.getChildFragmentManager()!!.fragments[0]

            when (menuItem.itemId) {
                R.id.profileFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is ProfileFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.notificationFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is NotificationsFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.homeFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is HomeFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.myOrderFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is MyOrderFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.offerFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is OffeersFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.mySubscriptionFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is MySubscriptionFragment) {
                        NavigationUI.onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.logout -> {
                    this.toast("Logout Clicked")
                    drawer.closeDrawers()
                    logOut()
                    false
                }

                else -> {
                    drawer.closeDrawers()
                    false
                }

            }
        }
    }

    //Setting Up the back button
    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.fragment),
            drawer
        )
    }

    fun hideBackBtn() {
        supportActionBar!!.setHomeButtonEnabled(false); // disable the button
        supportActionBar!!.setDisplayHomeAsUpEnabled(false); // remove the left caret
        supportActionBar!!.setDisplayShowHomeEnabled(false); // remove the icon
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish()
            return
        }
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                super.onBackPressed()
            }
        }

    }

    fun logOut() {
        showAlertDialog(this, "", "Are you sure, you want to logOut?",
            DialogInterface.OnClickListener { dialog, which ->
                if (which == DialogInterface.BUTTON_POSITIVE) {

                    if (prefs.getString(LOGIN_FROM).equals("Gplus")) {
                        mGoogleSignInClient.signOut()
                            .addOnCompleteListener(this) {
                                clearData()
                            }
                    } else if (prefs.getString(LOGIN_FROM).equals("InApp")) {
                        clearData()
                    }


                } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss()
                }
            })
    }

    fun clearData() {
        prefs.clearAllPref()
        Coroutines.io() {
            viewModel.deleteUser()
        }
        navigateToPreLoginPage(this)
    }
}
