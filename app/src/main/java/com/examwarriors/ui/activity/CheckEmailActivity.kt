package com.examwarriors.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.examwarriors.R
import com.examwarriors.databinding.ActivityCheckEmailBinding
import com.examwarriors.ui.modelfactory.CheckEmailViewModelFactory
import com.examwarriors.ui.viewmodel.CheckEmailViewModel
import com.examwarriors.utils.*
import com.examwarriors.utils.Constants.EMAIL
import com.examwarriors.utils.ModuleMaster.navigateToAuthActivity
import com.examwarriors.utils.ModuleMaster.navigateToPreLoginPage
import kotlinx.android.synthetic.main.activity_check_email.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class CheckEmailActivity : AppCompatActivity(), KodeinAware {

    private lateinit var toolbar: Toolbar

    override val kodein by kodein()
    private val factory: CheckEmailViewModelFactory by instance()
    var email: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityCheckEmailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_check_email)

        val viewModel = ViewModelProviders.of(this, factory).get(CheckEmailViewModel::class.java)

        binding.viewModel = viewModel;

        if (intent.getStringExtra(EMAIL) != null) {
            email = intent.getStringExtra(EMAIL)
            viewModel.email = email
        }

        setToolbar()

        viewModel.onStarted.observe(this, Observer {
            progress_bar.show()
        })

        viewModel.onFailure.observe(this, Observer {
            Coroutines.main {
                progress_bar.hide()
                root_layout.showSnackBar(it)
            }
        })
        viewModel.onSuccess.observe(this, Observer {
            Coroutines.main {
                progress_bar.hide()
                if (it != null) {
                    navigateToAuthActivity(this, it)
                }
            }
        })
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = ""
        toolbar.setBackgroundColor(resources.getColor(R.color.page_bg_color))
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_black)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        navigateToPreLoginPage(this)
    }
}
