package com.examwarriors.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.examwarriors.R
import com.examwarriors.data.db.entities.User
import com.examwarriors.databinding.ActivityPreLoginBinding
import com.examwarriors.ui.viewmodel.PreLoginViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import android.util.Log
import androidx.lifecycle.Observer
import com.examwarriors.data.preferences.LOGIN_FROM
import com.examwarriors.data.preferences.PreferenceProvider
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.utils.Constants.EMAIL
import com.examwarriors.utils.Constants.NAME
import com.examwarriors.utils.Constants.PROFILE_PIC
import com.examwarriors.utils.Coroutines
import com.examwarriors.utils.ModuleMaster.navigateToMainActivity
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class PreLoginActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val repository: UserRepository by instance()
    private val prefs: PreferenceProvider by instance()

    lateinit var viewModel: PreLoginViewModel
    lateinit var mGoogleSignInClient: GoogleSignInClient
    private val RC_SIGN_IN = 0

    val TAG: String = PreLoginActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val dataBinding: ActivityPreLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_pre_login)

        viewModel = ViewModelProviders.of(this).get(PreLoginViewModel::class.java)
        dataBinding.viewModel = viewModel

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(resources.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        viewModel.logInWithSignIn.observe(this, Observer {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode === RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }

    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            if (account != null)
                getProfileInfo(account)

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode())
        }

    }

    override fun onStart() {
        super.onStart()

        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (account != null)
            getProfileInfo(account)

    }

    private fun getProfileInfo(account: GoogleSignInAccount) {
        var user = User()
        user.name = account!!.displayName
        user.email = account!!.email
        var socialId = account!!.id
        var token = account.idToken
        user.userImageURL = account!!.photoUrl.toString()
        Coroutines.io() {
            repository.saveUser(user)
        }

        Log.v(TAG, "hh yashal token is $token")
        Log.v(TAG, "hh yashal token is ${account!!.displayName}")
        Log.v(TAG, "hh yashal token is ${account!!.email}")
        Log.v(TAG, "hh yashal token is $socialId")

        prefs.put(LOGIN_FROM, "Gplus")
        navigateToMainActivity(this, user)
    }

}
