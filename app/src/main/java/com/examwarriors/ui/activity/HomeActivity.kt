package com.examwarriors.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.examwarriors.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*
import android.widget.TextView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import com.examwarriors.ui.fragment.ProfileFragment
import com.examwarriors.ui.fragment.QuotesFragment
import android.widget.Toast
import androidx.core.view.GravityCompat
import android.os.Handler
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.examwarriors.utils.CircleTransform


class HomeActivity : AppCompatActivity() {

    lateinit var name: String
    lateinit var email: String
    lateinit var drawer: DrawerLayout
    lateinit var navController: NavController
    private var doubleBackToExitPressedOnce = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)

        drawer = findViewById(R.id.drawerLayout)

        navController = Navigation.findNavController(this, R.id.fragment)
        NavigationUI.setupWithNavController(nav_view, navController)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val headerView = navigationView.getHeaderView(0)
        val navUserName = headerView.findViewById(R.id.header_name) as TextView
        val navUserEmail = headerView.findViewById(R.id.header_email) as TextView
        val navUserImage = headerView.findViewById(R.id.imageView) as ImageView

        if (intent.getStringExtra("name") != null) {
            name = intent.getStringExtra("name")
            navUserName.text = name
        }
        if (intent.getStringExtra("email") != null) {
            email = intent.getStringExtra("email")
            navUserEmail.text = email
        }
        if (intent.getStringExtra("profilePic") != null) {
            var profilePic = intent.getStringExtra("profilePic")
            if (!profilePic.isNullOrEmpty())
                Picasso.get()
                    .load(profilePic)
                    .placeholder(R.drawable.ic_app_logo)
                    .error(R.drawable.ic_app_logo)
                    .transform(CircleTransform())
                    .into(navUserImage);
        }

        setupDrawerContent(navigationView)

    }

    private fun setupDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener { menuItem ->
            drawerLayout.closeDrawers()
            val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment)
            var fragment = navHostFragment?.getChildFragmentManager()!!.fragments[0]
            drawer.closeDrawers()

            when (menuItem.itemId) {
                R.id.profileFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is ProfileFragment) {
                        onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }

                R.id.quotesFragment -> {
                    drawer.closeDrawers()
                    if (fragment !is QuotesFragment) {
                        onNavDestinationSelected(menuItem, navController)
                    } else {
                        false
                    }
                }
                else -> {
                    drawer.closeDrawers()
                    false
                }

            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.fragment),
            drawerLayout
        )
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish()
            return
        }
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                if (supportFragmentManager.backStackEntryCount == 1) {
                    doubleBackToExitPressedOnce = true
                    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT)
                        .show()

                    Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
                } else {
                    super.onBackPressed()
                }
            }
        }

    }
}
