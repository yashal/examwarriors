package com.examwarriors.ui.viewmodel

import android.app.Application
import android.content.Context
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.examwarriors.R
import com.examwarriors.data.db.entities.User
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.utils.*

class CheckEmailViewModel(
    application: Application,
    private val repository: UserRepository
) : AndroidViewModel(application) {

    var email: String? = null
    var onStarted: MutableLiveData<String> = MutableLiveData()
    var onFailure: MutableLiveData<String> = MutableLiveData()
    var onSuccess: MutableLiveData<User> = MutableLiveData()

    val context: Context = application.applicationContext

    fun proceedClick(view: View) {
        view.hideKeyboard()
        onStarted.postValue(context.resources.getString(R.string.on_started))
        if (email.isNullOrEmpty()) {
            onFailure.postValue(context.resources.getString(R.string.email_required))
            return
        }
        if (!isValidEmail(email.toString())) {
            onFailure.postValue(context.resources.getString(R.string.invalid_email))
            return
        }
        Coroutines.io() {
            try {
                val authResponse = repository.checkUser(email.toString())
                authResponse.result?.let {
                    it.email = email.toString()
                    onSuccess.postValue(it)
                    return@io
                }
            } catch (ex: ApiException) {
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                onFailure.postValue(ex.message.toString())
            }
        }
    }

}