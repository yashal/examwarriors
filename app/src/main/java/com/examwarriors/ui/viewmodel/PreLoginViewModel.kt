package com.examwarriors.ui.viewmodel

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Build
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.examwarriors.R
import com.examwarriors.utils.ModuleMaster.navigateToCheckEmailActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener



class PreLoginViewModel(application: Application) : AndroidViewModel(application) {

    var logInWithSignIn = MutableLiveData<Any>()

    fun onSignInWithEmailClicked(view: View) {
        navigateToCheckEmailActivity(view.context)
    }

    fun gPlusLoginClicked(view: View) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.context!!.let {
                addPermissionDialogMarshMallowForAccount(
                    it
                )
            }
        } else {
            logInWithSignIn.postValue("signInWithGplus")
        }
    }

    private fun addPermissionDialogMarshMallowForAccount(context: Context) {
        Dexter.withActivity(context as Activity)
            .withPermissions(
                Manifest.permission.GET_ACCOUNTS
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        //                            Toast.makeText(context, "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        logInWithSignIn.postValue("signInWithGplus")
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings
                        Toast.makeText(context, "Permission not granted!", Toast.LENGTH_SHORT)
                            .show()
                        //                            showSettingsDialog();
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            })
            .withErrorListener { Toast.makeText(context, "Error occurred! ", Toast.LENGTH_SHORT).show() }
            .onSameThread()
            .check()
    }


}