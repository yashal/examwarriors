package com.examwarriors.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.examwarriors.data.repository.QuotesRepository
import com.examwarriors.utils.lazyDeferred

class QuotesViewModel(
    application: Application,
    repository: QuotesRepository
) : AndroidViewModel(application) {

    val quote by lazyDeferred {
        repository.getQuotes()
    }

}
