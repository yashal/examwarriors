package com.examwarriors.ui.viewmodel

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import com.examwarriors.utils.ModuleMaster.navigateToLearningActivity

class HomeViewModel(
    application: Application
) : AndroidViewModel(application) {
    private val context = application.applicationContext

    fun onLearningClick(view: View) {

        navigateToLearningActivity(view.context)

    }
}