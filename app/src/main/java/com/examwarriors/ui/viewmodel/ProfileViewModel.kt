package com.examwarriors.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.examwarriors.data.repository.UserRepository

class ProfileViewModel(
    application: Application,
    repository: UserRepository
) : AndroidViewModel(application) {

    val user = repository.getUser()

}
