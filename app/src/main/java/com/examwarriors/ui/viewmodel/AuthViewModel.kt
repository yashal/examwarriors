package com.examwarriors.ui.viewmodel

import android.app.Application
import android.content.Context
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.examwarriors.R
import com.examwarriors.data.db.entities.User
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.utils.*
import java.util.ArrayList
import java.util.HashMap

class AuthViewModel(
    application: Application,
    private val repository: UserRepository
) : AndroidViewModel(application) {

    var password: String? = null
    var name: String? = null
    var phone: String = ""

    var onStarted: MutableLiveData<String> = MutableLiveData()
    var onFailure: MutableLiveData<String> = MutableLiveData()
    var onSuccessFullyRegistered: MutableLiveData<String> = MutableLiveData()
    var onSuccess: MutableLiveData<User> = MutableLiveData()
    val context: Context = application.applicationContext

    fun getLoggedInUser() = repository.getUser()

    suspend fun deleteUser() = repository.deleteUser()

    fun signInClick(user: User, view: View) {
        view.hideKeyboard()
        onStarted.postValue(context.resources.getString(R.string.on_started))
        if (password.isNullOrEmpty()) {
            onFailure.postValue(context.resources.getString(R.string.password_required))
            return
        }

        Coroutines.io() {
            try {
                val authResponse = repository.doUserLogin(user.email!!, password!!)
                authResponse.result?.let {
                    it.email = user.email.toString()
                    onSuccess.postValue(it)
                    repository.saveUser(it)
                    return@io
                }
            } catch (ex: ApiException) {
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                onFailure.postValue(ex.message.toString())
            }
        }

    }

    fun signUpClick(user: User, view: View) {
        view.hideKeyboard()
        onStarted.postValue(context.resources.getString(R.string.on_started))

        if (name.isNullOrEmpty()) {
            onFailure.postValue(context.resources.getString(R.string.name_required))
            return
        }
        if (password.isNullOrEmpty()) {
            onFailure.postValue(context.resources.getString(R.string.password_required))
            return
        }

        if (!phone.isNullOrEmpty()) {
            if (!validLoginCredentials(phone)) {
                return
            }
        }

        Coroutines.io() {
            try {
                val authResponse = repository.doUserSignUp(user.email!!, password!!, phone, name!!)
                /*authResponse.result?.let {
                    authlViewListener?.onSuccess(it, user.email!!)
                    repository.saveUser(it)
                    return@io
                }*/
                onSuccessFullyRegistered.postValue(user.email!!)
                return@io
            } catch (ex: ApiException) {
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                onFailure.postValue(ex.message.toString())
            }
        }
    }

    private fun validLoginCredentials(username: String): Boolean {
        val bValid: Boolean
        var verifyMobile = HashMap<Boolean, String>()
        verifyMobile = getMobileNumberVerification(username)
        val boolList = verifyMobile.keys
        val mainList = ArrayList(boolList)

        if (mainList[0]) {
            bValid = true
        } else {
            bValid = false
            onFailure.postValue(verifyMobile[false]!!)
        }

        return bValid
    }


}