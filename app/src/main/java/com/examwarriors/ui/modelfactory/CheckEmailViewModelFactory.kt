package com.examwarriors.ui.modelfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.ui.viewmodel.CheckEmailViewModel

class CheckEmailViewModelFactory(
    private val application: Application,
    private val repository: UserRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CheckEmailViewModel(application, repository) as T
    }
}