package com.examwarriors.ui.modelfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.examwarriors.data.repository.UserRepository
import com.examwarriors.ui.viewmodel.AuthViewModel

class AuthViewModelFactory(
    private val application: Application,
    private val repository: UserRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AuthViewModel(application, repository) as T
    }
}