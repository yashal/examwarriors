package com.examwarriors.ui.modelfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.examwarriors.data.repository.QuotesRepository
import com.examwarriors.ui.viewmodel.QuotesViewModel

class QuotesViewModelFactory(
    private val application: Application,
    private val repository: QuotesRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return QuotesViewModel(application, repository) as T
    }
}