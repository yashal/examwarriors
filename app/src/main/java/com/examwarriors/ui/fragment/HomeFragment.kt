package com.examwarriors.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import com.examwarriors.R
import com.examwarriors.databinding.FragmentHomeBinding
import com.examwarriors.ui.activity.MainActivity
import com.examwarriors.ui.modelfactory.HomeViewModelFactory
import com.examwarriors.ui.viewmodel.HomeViewModel
import com.examwarriors.utils.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class HomeFragment : Fragment(), KodeinAware {

    override val kodein by kodein()
    private val factory: HomeViewModelFactory by instance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding: FragmentHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        val viewModel: HomeViewModel =
            ViewModelProviders.of(this, factory).get(HomeViewModel::class.java)
        binding.model = viewModel

        context?.toast("Home is opened")
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        val mainActivity = activity as MainActivity
        mainActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        mainActivity.bottomNav.visibility = View.VISIBLE
//        mainActivity.toolbarTitle.text = "Home"
    }


}
